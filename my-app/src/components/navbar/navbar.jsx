import React from "react";
import {NavLink} from "react-router-dom";
import './navbar.css'
export default function NavBarComponent(){

    return (    
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#"><i className="bi bi-shield-fill-plus"></i></a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
               {/*  activeClassName="active" */}
                    <NavLink exact to="/"  activeClassName="active" className="nav-link " > <div  >Inicio</div></NavLink>
                    
                </div>
                </div>
            </div>
            </nav>
)

}