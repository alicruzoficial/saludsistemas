import React from "react";
import { useEffect ,useState } from 'react';
import axios from "axios";
import f from "./f.module.css"
export default function FiltroXEspe(props){
    const [especialidades ,setEspecialidades]=useState([]);
    
 
    useEffect(()=>{

        espe();
    },[])

    function espe(){
        axios.get('http://localhost:9000/api/especialidad')
            .then(res=>{
                //console.log(res.data)
              
                setEspecialidades(res.data);
            })  
            .catch(e=>{
                console.log(e)
            })
    }
    function modificarChar(e){
        props.setValor(e)
        console.log(e);
         
    }


    return (    
        <select className={f.selectBreed } name="selectBreed" onChange={(event) => modificarChar(event.target.value)} >
            <option key={"selectBreed"} selected disabled>
                Selecione
            </option>
            {especialidades.map((name) => (
                <option key={name.id_espe} value={name.tipo}>
                {name.tipo}
                </option>
            ))} 
        </select>
)

}