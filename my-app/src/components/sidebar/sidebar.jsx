import React from "react";
import FiltroXcalle from "../filters/filtroXcalle";
import FiltroXEspe from "../filters/filtroXespecialidad";
import './sidebarmodule.css'

export default function SideBar(props) {
  return (
    <div className="sideBar">
      <div className="search">
        <span className="searchDogs"> Search a breed </span>
            busqueda
      </div>
    
      <div className="filters">
        <label>Especialidad</label>
            <FiltroXEspe  valor={props.valor} setValor={props.setValor}/>
  
      </div>


      <div className="filters">
        <label>Calle</label>
            <FiltroXcalle/>
  
      </div>

      
    </div>
  );
}
