import React from "react";
import pag from "./pagination.module.css"
import { useEffect ,useState } from 'react';
import axios from "axios";
import CardMedicoComponent from "../cardMedico/cardMedico";

export default function PaginationComponent(props){
    const [profesionales ,setProfesionales]=useState([]);
    const [filtro,setFiltro] = useState([])
 
    useEffect(()=>{

            prof();
    },[])

    function prof(){
        axios.get('http://localhost:9000/api/profesional')
            .then(res=>{
                setProfesionales(res.data);
            })  
            .catch(e=>{
                console.log(e)
            })
    }
    useEffect(() => {

        const cosa = profesionales.filter(x=>x.especialidad==props.valor)
        console.log(cosa);
        setFiltro(cosa)

    }, [props.valor]); 

    return (        
        <div className={pag.containerPag}>
                    {filtro.map((name) => (
                        <CardMedicoComponent id_prof={name.id_prof} nombre={name.nombre} especialidad={name.especialidad} />
                    ))} 
        </div>


)

}