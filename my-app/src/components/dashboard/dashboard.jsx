import React from "react";
import PaginationComponent from "../pagination/pagination";
import SideBar from "../sidebar/sidebar";
import das from './dashboard.module.css'
import { useState } from 'react';


export default function DashboardComponent(){
    
    const [valor,setValor]=useState('hola muddo');

    return (    
        <div className={das.containerDash}>   
            <SideBar valor={valor} setValor={setValor}/>
            <PaginationComponent valor={valor} />
        </div>
)

}