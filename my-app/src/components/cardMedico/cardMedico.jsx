import React from "react";
import {Link} from "react-router-dom";
import c from './cardMedico.module.css'

export default function CardMedicoComponent(props){

    return (    
        
             <>
                <div className = {c.name1}>
                    
                    <Link to={`/medicodetail/${props.id_prof}`}>
                        <p  className ={c.name} >{props.nombre}</p>  
                    </Link>
                    <img className ={c.pic} src={'https://image.flaticon.com/icons/png/512/843/843293.png'} alt={"img"}/> 

                    <div className = {c.divDietsNames}>
                    <h4>{props.especialidad}</h4>  
                    </div>   
                </div>
            </>
   
)

}