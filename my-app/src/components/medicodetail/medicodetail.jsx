import React from "react";
import './medicodetail.css'
import { useParams  } from "react-router";


export default function MedicodetailComponent(props){

    const parametro = useParams();
    
    const elemeto =props.profe.find((elemento)=>elemento.id_prof===parseInt(parametro.id));
   
    return (    
        <div>
            
               
                
                <div className="detailContainer">
                    <img className="detailedBreed" src={'https://image.flaticon.com/icons/png/512/843/843293.png'} ></img>
                        <div className="detailedBgWhite">
                            <div className="detailedBg2"></div>
                            <div className="detailedBg1"></div>
                            <div className="detailedInfo">
                            <h2>{elemeto.nombre}</h2>
                            <h3>matricula</h3>
                            <p>{elemeto.matricula}</p>
                            <h3>telefono</h3>
                            <p>{elemeto.telefono}</p>
                            <h3>especialidad</h3>
                            <p>{elemeto.especialidad}</p>
                            <h3>Direccion</h3>
                            <p>{elemeto.calle}</p>
                        </div>
                    </div>
                </div>
                

        </div>
)

}