
import './App.css';
import FooterComponent from './components/footer/footer';
import HeaderComponent from './components/header/header';
import NavBarComponent from './components/navbar/navbar';
import {Switch,Route} from "react-router-dom";
import Home from './components/home/home';
import MedicodetailComponent from './components/medicodetail/medicodetail';
import { useState ,useEffect } from "react";
import axios from "axios";


function App() {

  const [profe ,setProfe]=useState([]);
    
  function prof(){
      axios.get('http://localhost:9000/api/profesional')
          .then(res=>{
              setProfe(res.data);
          })  
          .catch(e=>{
              console.log(e)
          })
  }
  useEffect(()=>{

      prof();
  },[])
    

  return (
    <div className="App">


        <HeaderComponent/>
        <NavBarComponent/>
        <Switch>
          <Route exact path="/" >
                      <Home/>
          </Route>
          <Route exact path="/medicodetail/:id">
              <MedicodetailComponent profe={profe}/>
          </Route>
          <Route exact path="/medico">
                     "deas"
          </Route>

          <Route>
              <h1> PAG NOT FOUND 404</h1>
          </Route>
        </Switch>
        <FooterComponent></FooterComponent>
    </div>
  );
}

export default App;
