package salud.edu.ar.sulud.repository;

import org.springframework.data.repository.CrudRepository;

import salud.edu.ar.sulud.entity.Domicilio;

public interface DomicilioRepository extends CrudRepository<Domicilio ,Long>{

}
