package salud.edu.ar.sulud.service;

import java.util.List;

import salud.edu.ar.sulud.Dto.EspecialidadDTO;

public interface IEspecilidadService {
	
	public void save(EspecialidadDTO especialidadto);
	
	public List<EspecialidadDTO> getList();
	
	public void delete(Long id);
	
	public EspecialidadDTO findOne(Long id);
}
