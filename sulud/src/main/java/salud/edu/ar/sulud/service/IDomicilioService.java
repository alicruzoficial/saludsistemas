package salud.edu.ar.sulud.service;

import java.util.List;

import salud.edu.ar.sulud.Dto.DomicilioDTO;

public interface IDomicilioService {

	public void save(DomicilioDTO domiciliodto);
	
	public List<DomicilioDTO> getList();
	
	public void delete(Long id);
	
	public DomicilioDTO findOne(Long id);
}
