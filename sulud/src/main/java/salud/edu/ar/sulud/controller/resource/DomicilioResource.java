package salud.edu.ar.sulud.controller.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import salud.edu.ar.sulud.Dto.DomicilioDTO;

import salud.edu.ar.sulud.service.IDomicilioService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class DomicilioResource {
	private final Logger log = LoggerFactory.getLogger(ProfesionalResource.class);
	@Autowired
	IDomicilioService domicilioService ;
	
	//mostrar todos los clientes
	@GetMapping("/domicilio")
	public List<DomicilioDTO> getAllDomicilios(){
		log.debug("API REST buscando todos los domicilios");
		return domicilioService.getList();
	}
	//buscar por id
	@GetMapping("/domicilio/{id}")
		public DomicilioDTO getDomicilio(@PathVariable Long id) {
		log.debug("API REST buscando EL domicilio con el id =  [" + id + "]");
		return domicilioService.findOne(id);	
	}
}
