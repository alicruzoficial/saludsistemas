package salud.edu.ar.sulud.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import salud.edu.ar.sulud.Dto.ProfesionalDTO;
import salud.edu.ar.sulud.entity.Profesional;
import salud.edu.ar.sulud.repository.DomicilioRepository;
import salud.edu.ar.sulud.repository.EspecialidadRepository;
import salud.edu.ar.sulud.repository.ProfesionalRepository;


@Service
public class ProfesionalServiceImpl implements IProfesionalService{
	
	@Autowired
	private ProfesionalRepository profesionalRepository;
	@Autowired 
	private DomicilioRepository domicilioRepository;
	
	@Autowired 
	private EspecialidadRepository EspecialidadRepository;
	
	private ModelMapper mapper;
	
	public ProfesionalServiceImpl() {
		mapper = new ModelMapper();
	}
	
	
	
	@Override
	public void save(ProfesionalDTO profecionaldto) {
		// TODO Auto-generated method stub
	}
	
	
	@Override
	public List<ProfesionalDTO> getList() {
		// TODO Auto-generated method stub
		ProfesionalDTO pdto =new ProfesionalDTO();
		List<ProfesionalDTO> usuarios = new ArrayList<>();
		
		for (Profesional usuario : profesionalRepository.findAll()){
			if (usuario != null) {
				
				pdto.setId_prof(usuario.getId_prof());
				pdto.setNombre(usuario.getNombre());  
				pdto.setApellido(usuario.getApellido());
				pdto.setMatricula(usuario.getMatricula());
				pdto.setTelefono(usuario.getTelefono());
				pdto.setId_dom(usuario.getDomicilio().getId_dom());
				pdto.setId_espe(usuario.getEspecialidad().getId_espe());
				pdto.setCalle(usuario.getDomicilio().getCalle());
				pdto.setEspecialidad(usuario.getEspecialidad().getTipo());
				
				usuarios.add(pdto);
				pdto = new ProfesionalDTO();
			}
			
		}
		return (usuarios);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProfesionalDTO findOne(Long id) {
		ProfesionalDTO pdto =new ProfesionalDTO();
		Optional<Profesional> usuario = profesionalRepository.findById(id);
		if (usuario.isPresent()) {
			pdto.setId_prof(usuario.get().getId_prof());
			pdto.setNombre(usuario.get().getNombre());  
			pdto.setApellido(usuario.get().getApellido());
			pdto.setMatricula(usuario.get().getMatricula());
			pdto.setTelefono(usuario.get().getTelefono());
			pdto.setId_dom(usuario.get().getDomicilio().getId_dom());
			pdto.setId_espe(usuario.get().getEspecialidad().getId_espe());
			pdto.setCalle(usuario.get().getDomicilio().getCalle());
			pdto.setEspecialidad(usuario.get().getEspecialidad().getTipo());
			
			return pdto;
		}
		return null;
	}
	
	private ProfesionalDTO mapToDTO(Profesional usuario) {
		
		return mapper.map(usuario, ProfesionalDTO.class);
	}
	
	private Profesional mapToEntity(ProfesionalDTO usuarioDTO) {
		return mapper.map(usuarioDTO, Profesional.class);
	}



	@Override
	public List<ProfesionalDTO> search(String filtro) {
		// TODO Auto-generated method stub
		
		List<Profesional> usuarioprofes = profesionalRepository.search(filtro);
		ProfesionalDTO pdto =new ProfesionalDTO();
		List<ProfesionalDTO> usuarios = new ArrayList<>();
		
		for (Profesional usuario : usuarioprofes){
			if (usuario != null) {
				
				pdto.setId_prof(usuario.getId_prof());
				pdto.setNombre(usuario.getNombre());  
				pdto.setApellido(usuario.getApellido());
				pdto.setMatricula(usuario.getMatricula());
				pdto.setTelefono(usuario.getTelefono());
				pdto.setId_dom(usuario.getDomicilio().getId_dom());
				pdto.setId_espe(usuario.getEspecialidad().getId_espe());
				pdto.setCalle(usuario.getDomicilio().getCalle());
				pdto.setEspecialidad(usuario.getEspecialidad().getTipo());
				
				usuarios.add(pdto);
				pdto = new ProfesionalDTO();
			}
			
		}
		return (usuarios);
	}
	
}
