package salud.edu.ar.sulud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuludApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuludApplication.class, args);
	}

}
