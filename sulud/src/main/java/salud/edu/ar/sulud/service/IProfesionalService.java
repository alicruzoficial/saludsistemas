package salud.edu.ar.sulud.service;

import java.util.List;

import salud.edu.ar.sulud.Dto.ProfesionalDTO;

public interface IProfesionalService {

	public void save(ProfesionalDTO profecionaldto);
	
	public List<ProfesionalDTO> getList();
	
	public void delete(Long id);
	
	public ProfesionalDTO findOne(Long id);
	
	public List<ProfesionalDTO> search(String filtro);
}
