package salud.edu.ar.sulud.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import salud.edu.ar.sulud.Dto.DomicilioDTO;
import salud.edu.ar.sulud.entity.Domicilio;
import salud.edu.ar.sulud.repository.DomicilioRepository;


@Service
public class DomicilioServiceImpl implements IDomicilioService{
	
	
	@Autowired 
	private DomicilioRepository domicilioRepository;
	
	private ModelMapper mapper;
	
	public DomicilioServiceImpl() {
		mapper = new ModelMapper();
	}
	
	@Override
	public void save(DomicilioDTO domiciliodto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<DomicilioDTO> getList() {
		// TODO Auto-generated method stub
		List<DomicilioDTO> usuarios = new ArrayList<>();
		
		for (Domicilio usuario : domicilioRepository.findAll()){
			if (usuario != null) {
				usuarios.add(mapToDTO(usuario));
			}
		}
		return (usuarios);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DomicilioDTO findOne(Long id) {
		Optional<Domicilio> usuario = domicilioRepository.findById(id);
		if (usuario.isPresent()) {
			return mapToDTO(usuario.get());
		}
		return null;
	}
	
	private DomicilioDTO mapToDTO(Domicilio usuario) {
		return mapper.map(usuario, DomicilioDTO.class);
	}
	
	private Domicilio mapToEntity(DomicilioDTO usuarioDTO) {
		return mapper.map(usuarioDTO, Domicilio.class);
	}
	
}
