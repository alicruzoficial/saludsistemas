package salud.edu.ar.sulud.Dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProfesionalDTO implements Serializable  {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "id_prof")
	private Long id_prof;
	
	@JsonProperty(value = "matricula")
	private String matricula;
	
	@JsonProperty(value = "nombre")
	private String nombre;
	
	@JsonProperty(value = "apellido")
	private String apellido;
	
	@JsonProperty(value = "telefono")
	private String telefono;
	
	@JsonProperty(value = "id_dom")
	private Long id_dom;

	@JsonProperty(value = "id_espe")
	private Long id_espe;
	
	@JsonProperty(value = "especialidad")
	private String especialidad;
	
	@JsonProperty(value = "calle")
	private String calle;

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	
	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Long getId_espe() {
		return id_espe;
	}

	public void setId_espe(Long id_espe) {
		this.id_espe = id_espe;
	}

	public Long getId_prof() {
		return id_prof;
	}

	public void setId_prof(Long id_prof) {
		this.id_prof = id_prof;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Long getId_dom() {
		return id_dom;
	}

	public void setId_dom(Long id_dom) {
		this.id_dom = id_dom;
	}
	
	
}
