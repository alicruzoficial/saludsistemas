package salud.edu.ar.sulud.repository;
import org.springframework.data.repository.CrudRepository;

import salud.edu.ar.sulud.entity.Especialidad;
public interface EspecialidadRepository extends CrudRepository<Especialidad ,Long> {

}
