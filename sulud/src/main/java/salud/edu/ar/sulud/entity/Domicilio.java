package salud.edu.ar.sulud.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "domicilio")
public class Domicilio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_dom;
	
	private String calle;
	
	private String numero;
	
	private String barrio;
	
	private String localidad;
	
	@OneToMany(mappedBy = "domicilio",cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH , CascadeType.REFRESH})
    private List<Profesional> profesional;

	public void addProfesional(Profesional prof){
	    if(prof ==null)  profesional = new ArrayList<>();
	    profesional.add(prof);
	    prof.setDomicilio(this);
	}
	
	public Domicilio() {
		super();
	}

	public Long getId_dom() {
		return id_dom;
	}

	public void setId_dom(Long id_dom) {
		this.id_dom = id_dom;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public List<Profesional> getProfecional() {
		return profesional;
	}

	public void setProfecional(List<Profesional> profecional) {
		this.profesional = profecional;
	}
	
}
