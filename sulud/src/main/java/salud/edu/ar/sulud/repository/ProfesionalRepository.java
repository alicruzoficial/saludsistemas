package salud.edu.ar.sulud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import salud.edu.ar.sulud.entity.Profesional;

public interface ProfesionalRepository extends CrudRepository<Profesional,Long>{
	
	@Query(value="SELECT p FROM Profesional p WHERE p.nombre LIKE %:filtro% OR p.apellido LIKE %:filtro%")     
	List<Profesional> search(@Param("filtro") String filtro);
}
