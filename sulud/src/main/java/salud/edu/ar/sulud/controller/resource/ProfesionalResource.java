package salud.edu.ar.sulud.controller.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import salud.edu.ar.sulud.service.IProfesionalService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import salud.edu.ar.sulud.Dto.ProfesionalDTO;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ProfesionalResource {
	private final Logger log = LoggerFactory.getLogger(ProfesionalResource.class);
	
	@Autowired
	IProfesionalService profesionalservice;
	
	//mostrar todos los clientes
	@GetMapping("/profesional")
	public List<ProfesionalDTO> getAllUsuarios(){
		log.debug("API REST buscando todos los profecionales");
		return profesionalservice.getList();
	}
	//buscar por id
	@GetMapping("/profesional/{id}")
		public ProfesionalDTO getUsuario(@PathVariable Long id) {
		log.debug("API REST buscando EL profecionales con el id =  [" + id + "]");
		return profesionalservice.findOne(id);	
	}
	@GetMapping("/profesional/name/{filtro}")
	public List<ProfesionalDTO> search(@PathVariable String filtro) {
	filtro = filtro.toUpperCase();
	log.debug("API REST buscando EL profecionales con el nombre o apellido=  ["+ filtro +"]");
	return profesionalservice.search(filtro);	
}
}
