package salud.edu.ar.sulud.controller.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import salud.edu.ar.sulud.Dto.EspecialidadDTO;
import salud.edu.ar.sulud.service.IEspecilidadService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class EspecialidadResource {
	private final Logger log = LoggerFactory.getLogger(ProfesionalResource.class);
	
	@Autowired
	IEspecilidadService especilidadService;
	
	//mostrar todos los clientes
	@GetMapping("/especialidad")
	public List<EspecialidadDTO> getAllEspecialidades(){
		log.debug("API REST buscando todos los especialidad");
		return especilidadService.getList();
	}
	//buscar por id
	@GetMapping("/especialidad/{id}")
		public EspecialidadDTO getEspecialidad(@PathVariable Long id) {
		log.debug("API REST buscando EL especialidad con el id =  [" + id + "]");
		return especilidadService.findOne(id);	
	}	
}
