package salud.edu.ar.sulud.Dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import salud.edu.ar.sulud.entity.Profesional;

public class EspecialidadDTO implements Serializable  {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "id_espe")
	private Long id_espe;
	
	@JsonProperty(value = "tipo")
	private String tipo;
	
	@JsonProperty(value = "profesional")
	private List<Profesional> profesional;

	public Long getId_espe() {
		return id_espe;
	}

	public void setId_espe(Long id_espe) {
		this.id_espe = id_espe;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Profesional> getProfesional() {
		return profesional;
	}

	public void setProfesional(List<Profesional> profesional) {
		this.profesional = profesional;
	}
	
	
	
}
