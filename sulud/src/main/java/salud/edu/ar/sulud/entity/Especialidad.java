package salud.edu.ar.sulud.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "especialidad")
public class Especialidad {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_espe;
	
	private String tipo;
	
	@OneToMany(mappedBy = "especialidad",cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH , CascadeType.REFRESH})
    		private List<Profesional> profesional;

	public void addProfesional(Profesional prof){
	    if(prof ==null)  profesional = new ArrayList<>();
	    profesional.add(prof);
	    prof.setEspecialidad(this);
	}
	
	public Especialidad() {

	}

	public Long getId_espe() {
		return id_espe;
	}

	public void setId_espe(Long id_espe) {
		this.id_espe = id_espe;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Profesional> getProfecional() {
		return profesional;
	}

	public void setProfecional(List<Profesional> profecional) {
		this.profesional = profecional;
	}
}
