package salud.edu.ar.sulud.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import salud.edu.ar.sulud.Dto.EspecialidadDTO;
import salud.edu.ar.sulud.entity.Especialidad;
import salud.edu.ar.sulud.repository.EspecialidadRepository;


@Service
public class EspecialidadServiceImpl implements IEspecilidadService{

	@Autowired 
	private EspecialidadRepository especialidadRepository;
	
	private ModelMapper mapper;
	
	public EspecialidadServiceImpl() {
		mapper = new ModelMapper();
	}
	
	@Override
	public void save(EspecialidadDTO especialidadto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EspecialidadDTO> getList() {
		// TODO Auto-generated method stub
		List<EspecialidadDTO> usuarios = new ArrayList<>();
		
		for (Especialidad usuario : especialidadRepository.findAll()){
			if (usuario != null) {
				usuarios.add(mapToDTO(usuario));
			}
		}
		return (usuarios);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EspecialidadDTO findOne(Long id) {
		// TODO Auto-generated method stub
		Optional<Especialidad> usuario = especialidadRepository.findById(id);
		if (usuario.isPresent()) {
			return mapToDTO(usuario.get());
		}
		return null;
	}
	
	private EspecialidadDTO mapToDTO(Especialidad usuario) {
		return mapper.map(usuario, EspecialidadDTO.class);
	}
	
	private Especialidad mapToEntity(EspecialidadDTO usuarioDTO) {
		return mapper.map(usuarioDTO, Especialidad.class);
	}

}
