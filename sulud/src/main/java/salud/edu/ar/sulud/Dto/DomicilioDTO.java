package salud.edu.ar.sulud.Dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import salud.edu.ar.sulud.entity.Profesional;

public class DomicilioDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "id_dom")
	private Long id_dom;
	
	@JsonProperty(value = "calle")
	private String calle;
	
	@JsonProperty(value = "numero")
	private String numero;
	
	@JsonProperty(value = "barrio")
	private String barrio;
	
	@JsonProperty(value = "localidad")
	private String localidad;
	
	@JsonProperty(value = "profesional")
	private List<Profesional> profesional;

	public Long getId_dom() {
		return id_dom;
	}

	public void setId_dom(Long id_dom) {
		this.id_dom = id_dom;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public List<Profesional> getProfesional() {
		return profesional;
	}

	public void setProfesional(List<Profesional> profesional) {
		this.profesional = profesional;
	}
	
	
}
