package salud.edu.ar.sulud;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import salud.edu.ar.sulud.entity.Domicilio;
import salud.edu.ar.sulud.entity.Profesional;
import salud.edu.ar.sulud.repository.DomicilioRepository;
import salud.edu.ar.sulud.repository.ProfesionalRepository;



@SpringBootTest
class SuludApplicationTests {
	@Autowired
	ProfesionalRepository repo;
	
	@Test
	void contextLoads() {
		Profesional prof = new Profesional() ;
		prof.setApellido("cruz");
		prof.setNombre("ali");
		prof.setMatricula("1002");
		prof.setTelefono("388430981");
		
		repo.save(prof);
	}

}
